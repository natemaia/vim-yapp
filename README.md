# vim-yapp

---
yapp - yet another pair plugin

Aims to improve insertion of paired characters and tags when scripting, coding, or writing markup.

Uses sane defaults and simple heuristics to determine when a completion should happen.
The current filetype, surrounding text, syntax, current pair count, and was `<CR>` or `<Space>` expanded.


Most settings are configurable in your `.vimrc` or `init.vim`, see `:h yapp`.


## Setup

------------

#### Vim-plug

```
Plug 'https://bitbucket.org/natemaia/vim-yapp'
```

#### Pathogen

```
mkdir -p ~/.vim/bundle
git clone https://bitbucket.org/natemaia/vim-yapp ~/.vim/bundle/vim-yapp
```

#### Vim *version 8.0 or greater*

```
mkdir ~/.vim/pack/plugins/start
git clone https://bitbucket.org/natemaia/vim-yapp ~/.vim/pack/plugins/start/vim-yapp
```

## Examples and Demos
---

Basic paired character completion of `(), {}, [], "", ''` and \`\` across all file types

![Basic](https://i.imgur.com/3D11pLS.gif)


Triple quote and grave (back tick) completion for python docstrings and markdown code-blocks.

![Quotes/grave](https://i.imgur.com/iff4SeT.gif)


Basic tag and `<>` completion for html, xml, vim, c, and c++ filetypes

![Tags](https://i.imgur.com/fmsADVd.gif)

---

Completion of paired characters based on previously entered and surrounding text.

*In the below examples `|` represents the cursor location.*


- Insert pairs:

        insert: [
        result: [|]

        insert: #include <
        result: #include <|>

        insert: "
        result: "|"

        insert: '''
        result: '''|'''


- Delete pairs:

        insert: foo[|] (press <BS> at |)
        result: foo

        input:
        {
        |   (press <BS> at |)
        }
        output: {|}


- Insert newline after `<CR>`:

        insert: {|} (press <CR> at |)
        result:
        {
            |
        }

        insert: ```|```  (press <CR> at |)
        result:
        ```
        |
        ```


- Insert extra space inside '[]', '()', and '{}':

        insert: {|} (press <SPACE> at |)
        result: { | }


        insert: {|} (press `<SPACE>foo}` at |)
        result: { foo }|


        insert: '|' (press <SPACE> at |)
        result: ' |'


- Skip pairing when previous character is '\':

        insert: "\'
        result: "\'"


- Smart completion around 'word' characters, nested quotes, and non-whitespace:

        insert: foo| (press ' at |)
        result: foo'

        insert: |foo (press ' at |)
        result: 'foo

        insert: |foo (press ( at |)
        result: (foo

        insert: foo|; (press ( at |)
        result: foo(|);

        insert: "|" (press ' at |)
        result: "'|"


- Support for python f-strings and b-strings:

        insert: f|  (press ' at |)
        result: f'|'

        insert: f|  (press " at |)
        result: f"|"



- Jump past closing pair character:

        insert: []|
        result: []|

        insert: "|"  (press " at |)
        result: ""|

        insert: [ | ]  (press ] at |)
        result: [ ]|

        insert:
        {
            code;|  (press } at |)
        }
        result:
        {
            code;
        }|


- Html/Xml tag completion:

        insert: <h1>
        result: <h1>|</h1>

        insert: <div>
        result:
            <div>
                |
            </div>

        result:
            <div>
                |  (press > at |)
            </div>
        result:
            <div>

            </div>|


Caveats
---

Currently the plugin overrides `<Space>` which in turn breaks abbreviations.

To fix this you can wrap it in an expression mapping (note: it needs to be an
imap for expr to work), this also has the nasty side effect of adding a
trailing space to your abbreviation, see further below for a better solution.

    imap <expr><space> "\<C-]>\<Space>"


I use the following in combination with [neosnippet](https://github.com/shougo/neosnippet.vim)
to expand a snippet after typing it's trigger and pressing space. The expansion only occurs
when the cursor is at the end of the line and the current syntax isn't comment or string
that way when editing exiting text I avoid unwanted expansions.

    " consume the last character if it matches pattern pat
    function! Eatchar(pat) abort
        let c = nr2char(getchar(0))
        return (c =~ a:pat) ? '' : c
    endfunc


    " expand an alias or snippet to the left of the cursor
    function! Expanded_space() abort
        let l:col = col('.') - 1

        " the current text syntax under the cursor is not a comment, string, or doc
        if synidattr(synid(line('.'), l:col, 0), 'name') !~? '\vomment|tring|doc'

            " a snippet is expandable and the cursor is at the end of the current line
            if l:col == len(getline('.')) && neosnippet#expandable()
                return neosnippet#mappings#expand_impl()
            endif

        endif

        " base case always returns an abbreviation expansion sequence and the space pressed
        return "\<C-]>\<Space>"
    endfunction


    " imap <Space> to the function
    imap <silent><expr> <Space> Expanded_space()


    " any insertmode abbreviations followed by <C-r>=Eatchar('\s')<cr>
    " or just Eatchar('\s') in expression abbreviations
    " consume the trailing space added by the Expanded_space() function

    inoreabbr <silent> timef <C-R>=strftime('%a, %d %b %y')<CR><Space>{{{<CR><CR>}}}<Up><C-R>=Eatchar('\s')<CR>

    inoreabbr <silent> foldv ------ text ------ {{{<CR><CR>}}}<Up><BS><C-R>=Eatchar('\s')<CR><CR>

    inoreabbr <silent><expr> #!! "#!/usr/bin/env" . (empty(&ft) ? '' : (&ft == 'sh') ? " bash\<CR>" : ' ' . &ft) . Eatchar('\s')

