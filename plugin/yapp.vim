" yapp.vim: Completion of paired characters and xml/html tags
" written by: Nathaniel Maia, 2018-2019

if exists('g:loaded_yapp') || &compatible " {{{1
    finish
endif
let g:loaded_yapp = 1


function! Yapp_try_init() abort " {{{1
    if exists('b:loaded_yapp')
        return
    endif
    let b:loaded_yapp  = 1
    let b:used_return  = 0
    let b:used_space   = 0
    let b:pair_count   = 0
    let b:save_count   = 0
    let b:save_return  = 0
    let b:last_comptag = ''
    let b:save_comptag = ''
    if &filetype =~? '\vxml|html|vim|c|cpp'
        let g:yapp_pairs['<'] = '>'
    endif
    let b:yapp_pairs = exists('b:yapp_pairs') ? b:yapp_pairs : g:yapp_pairs
    if g:yapp_bind_enter
        call <SID>create_plug_map('<CR>', 'Yapp_enter', 'OriginalCR')
    endif
    if g:yapp_bind_backspace
        execute 'inoremap <buffer><silent> <BS> <C-R>=Yapp_backspace()<CR>'
    endif
    if g:yapp_bind_space
        call <SID>create_plug_map('<Space>', 'Yapp_space', 'OriginalSpace')
    endif
    if g:yapp_bind_cursormove
        call <SID>create_plug_map('<Left>', 'Yapp_cursormove', 'OriginalRight')
        call <SID>create_plug_map('<Right>', 'Yapp_cursormove', 'OriginalLeft')
    endif
    for [l:l, l:r] in items(b:yapp_pairs)
        call <SID>create_pair_map(l:l, l:r)
    endfor
endfunction


function! Yapp_pair(key) abort " {{{1
    let s:col    = col('.')
    let s:row    = getline('.')
    let s:bf     = strpart(s:row, 0, s:col - 1)
    let s:af     = strpart(s:row, s:col - 1)
    let s:pchars = split(s:bf, '\zs')
    let s:nchars = split(s:af, '\zs')
    let s:cchar  = get(s:nchars,  0, '')
    let s:nchar  = get(s:nchars,  1, '')
    let s:pchar  = get(s:pchars, -1, '')
    let s:synid  = synIDattr(synID(line('.'), s:col - 1, 0), 'name')

    if s:pchar ==? '\' || s:synid =~? 'omment\|doc' || <SID>non_identical(b:yapp_pairs, a:key, s:cchar)
        return a:key
    elseif !has_key(b:yapp_pairs, a:key)
        return <SID>close_wrap(a:key)
    elseif a:key == b:yapp_pairs[a:key]
        return <SID>close_quote(a:key)
    elseif a:key =~# '<'
        if (&filetype =~# 'c\|cpp' && !(s:bf =~# '^\s*#include\s*')) || (&filetype =~# 'vim' && !(s:bf =~# '^\s*\(map\|call\|function\|union\)\s*$'))
            return a:key
        endif
    endif

    call Yapp_counter('inc')
    return a:key.b:yapp_pairs[a:key].s:left
endfunction


function! Yapp_enter() abort " {{{1
    if !Yapp_counter('exists')
        return ''
    endif

    let l:col   = col('.')
    let l:cline = getline('.')
    let l:pline = getline(line('.') - 1)
    let l:plen  = strlen(l:pline)
    let l:cchar = l:cline[l:col - 1]
    let l:pchar = l:pline[l:plen - 1]
    let l:af    = strpart(l:cline, l:col - 1, l:col + 2)
    let l:bf    = strpart(l:pline, l:plen - 3, l:plen)

    if <SID>has_matching_pair(b:yapp_pairs, l:pchar, l:cchar, 0) && <SID>is_triple_pair(l:pchar, l:cchar, l:af, l:bf)
        let b:used_return += 1
        let l:r = &equalprg !=# '' ? 'O' : '=ko'
        return "\<ESC>".l:r."\<C-R>=Yapp_counter()\<CR>"
    endif
    return ''
endfunction


function! Yapp_backspace() abort " {{{1
    if !Yapp_counter('exists')
        return "\<BS>"
    endif
    let l:col    = col('.')
    let l:line   = line('.')
    let l:row    = getline('.')
    let l:ppchar = get(split(strpart(l:row, 0, l:col - 1), '\zs'), -2, '')

    if l:ppchar !=? '\' && <SID>not_synid('omment')
        let l:nchar  = get(split(strpart(l:row, l:col - 1), '\zs'),  1, '')
        let l:cchar  = get(split(strpart(l:row, l:col - 1), '\zs'),  0, '')
        let l:pchar  = get(split(strpart(l:row, 0, l:col - 1), '\zs'), -1, '')
        let l:pline  = getline(l:line - 1)
        let l:nline  = getline(l:line + 1)
        let l:plchar = strpart(l:pline, len(l:pline) - 1, 1)

        if <SID>has_matching_pair(b:yapp_pairs, l:ppchar, l:nchar, 1) && b:used_space > 0
            let b:used_space -= 1
            return "\<BS>\<DEL>"
        elseif <SID>has_matching_pair(b:yapp_pairs, l:pchar, l:cchar, 0)
            call Yapp_counter('dec')
            return "\<BS>\<DEL>"
        elseif has_key(b:yapp_pairs, l:plchar) && b:used_return > 0 && l:col == 1
            if l:nline =~ '^\s*'.b:yapp_pairs[l:plchar] && l:cchar =~? '\s*$'
                let b:used_return -= 1
                return "\<BS>\<DEL>".repeat("\<DEL>", len(matchstr(l:nline, '^\s*')))
            endif
        endif
    endif
    return "\<BS>"
endfunction


function! Yapp_space() abort " {{{1
    if !Yapp_counter('exists')
        return ''
    endif
    let l:col   = col('.')
    let l:row   = getline('.')
    let l:bf    = strpart(l:row, 0, l:col - 1)
    let l:pchar = l:row[l:col - 3]
    let l:cchar = l:row[l:col - 1]

    if <SID>has_matching_pair(b:yapp_pairs, l:pchar, l:cchar, 1) && ! (&filetype =~# 'xml\|html|vim|cpp|c' && l:pchar ==? '<')
        let b:used_space += 1
        return "\<Space>".s:left
        " ciw@<Esc>"_s<C-R>"<Esc>b
    endif
    return ''
endfunction


function! Yapp_counter(...) abort  " {{{1
    let l:i = (a:0 >= 2) ? a:2 : 1
    if !exists('b:pair_count')
        let b:pair_count   = 0
        let b:used_return  = 0
        let b:used_space   = 0
        let b:last_comptag = ''
    endif
    if (a:0 == 0)
        let b:pair_count   = b:save_count
        let b:used_return  = b:save_return
        let b:last_comptag = b:save_comptag
    elseif a:1 ==? 'exists'
        return b:pair_count
    elseif a:1 ==? 'inc'
        let b:pair_count += l:i
    elseif a:1 ==? 'dec'
        let b:pair_count -= (b:pair_count > 0) ? l:i : 0
    elseif a:1 ==? 'reset'
        let b:save_count   = b:pair_count
        let b:save_return  = b:used_return
        let b:save_comptag = b:last_comptag
        let b:pair_count   = 0
        let b:used_return  = 0
        let b:used_space   = 0
        let b:last_comptag = ''
    endif
    return ''
endfunction


function! Yapp_cursormove() abort " {{{1
    call Yapp_counter('dec')
    return ''
endfunction


function! <SID>close_quote(key) abort " {{{1
    if Yapp_counter('exists')
        let l:jump = <SID>jump_over_pair(a:key)  " when the counter > 1
        if l:jump !=# ''
            call Yapp_counter('dec')
            return l:jump
        endif
    endif

    if &filetype ==? 'python' && a:key =~# "\v'|".'"' && (s:bf =~# '\v\s*<b$' || s:bf =~# '\v\s*<f$')
        call Yapp_counter('inc')  " python b-strings and f-strings
        return a:key.a:key.s:left
    elseif s:cchar =~? a:key.'\v|"|'."'" || s:cchar !~ '\v\w*|\$'
        return a:key
    elseif (a:key =~? '\v"|`' || a:key =~? "'") && (s:pchar =~? '\v\w' || s:cchar == a:key || s:cchar =~? '\v\w')
        return a:key
    elseif &filetype ==? 'vim' && a:key ==? '"' && (s:bf =~? '^\s*"\?$' || col('.') + 1 != col('$'))
        return a:key
    elseif &filetype =~? '\vscheme|lisp|elisp|clojure' && a:key =~? "\v'|`"
        return a:key
    elseif a:key =~? '\v"|`|'."'" && s:row[s:col - 3] == a:key && s:pchar == a:key && s:row[s:col - 4] != s:row[s:col - 3]
        call Yapp_counter('inc', 3)
        return repeat(a:key, 4).repeat(s:left, 3)  " triple pairing
    elseif (count(s:row, a:key) % 2 == 1)
        return a:key
    endif

    call Yapp_counter('inc')
    return a:key.a:key.s:left
endfunction


function! <SID>close_tag(key) abort " {{{1
    if s:bf =~? '\v\s*<\w*\s*' && s:pchar !=# '/'
        let l:row = getline('.')
        let l:col = col('.')
        let l:tag = matchstr(matchstr(l:row, '.*<\zs.\+\%'.l:col.'c'), '^[0-9A-Za-z_\-]*')
        let l:pos = searchpos('\v['.a:key.']', 'Wc')
        if l:tag !=# '' && l:tag !~? '\varea|base|col|embed|hr|img|input|keygen|link|param|source|track|separator|wbr|meta|br'
            let b:last_comptag = '</'.l:tag.'>'
            let l:jumpleft = repeat(s:left, len(l:tag) + 3)
            let l:fulltag  = s:right.'</'.l:tag.'>'.l:jumpleft
            if l:tag =~? '\vdiv|table|html|head|body|menu|action|item|span'
                let b:used_return += 1
                let l:r = (&equalprg !=# '') ? 'O' : '=ko'
                let l:fulltag .= "\<CR>\<ESC>".l:r."\<C-R>=Yapp_counter()\<CR>"
            endif
            return l:fulltag
        endif
    endif
endfunction


function! <SID>close_wrap(key) abort " {{{1
    if Yapp_counter('exists')
        if g:yapp_bind_tag && &filetype =~? 'html\|xml' && a:key ==? '>'
            let l:tag = <SID>close_tag(a:key)
            if l:tag !=# ''
                return l:tag
            endif
        endif

        let l:jump = <SID>jump_over_pair(a:key)
        if l:jump !=# ''
            call Yapp_counter('dec')
            return l:jump
        endif
    endif
    return a:key
endfunction


function! <SID>jump_over_pair(key) abort " {{{1
    if s:cchar == a:key
        return s:right
    elseif s:cchar =~? '\v\s' && s:nchar == a:key && b:used_space > 0
        let b:used_space -= 1
        return s:right.s:right
    elseif (s:cchar ==? '' && matchstr(getline(nextnonblank(line('.') + 1)), '\s*\zs.') == a:key)
                \  || (a:key ==# '>' && (&filetype =~# 'xml\|html' && (getline(line('.') + 1) =~# b:last_comptag || s:af =~# b:last_comptag)))
        let b:used_space -= b:used_space > 0 ? 1 : 0
        let b:used_return -= b:used_return > 0 ? 1 : 0
        let l:cursor_pos = searchpos('\v['.a:key.']', 'Wc')
        return s:right
    endif
    return ''
endfunction


function! <SID>not_synid(match) abort " {{{1
    return (synIDattr(synID(line('.'), col('.') - 1, 0), 'name') !~ a:match) ? 1 : 0
endfunction


function! <SID>is_triple_pair(current, next, after, before) abort " {{{1
    let l:tri = repeat(a:next, 3)
    if a:next != a:current || (a:next == a:current && a:after == l:tri && a:before == l:tri)
        return 1
    endif
    return 0
endfunction


function! <SID>has_matching_pair(dict, key, match, ignore_identical) abort " {{{1
    if has_key(a:dict, a:key) && a:match == a:dict[a:key] && <SID>not_synid('omment')
        return ((a:ignore_identical && a:key != a:match) || ! a:ignore_identical) ? 1 : 0
    endif
    return 0
endfunction


function! <SID>non_identical(dict, key, current) abort " {{{1
    return (has_key(a:dict, a:key) && a:key != a:dict[a:key] && a:current =~? '\v\w') ? 1 : 0
endfunction


function! <SID>create_pair_map(l, r) abort " {{{1
    let l:l  = a:l ==? '|' ? '<BAR>' : a:l
    let l:r  = a:r ==? '|' ? '<BAR>' : a:r
    let l:lk = substitute(l:l, "'", "''", 'g')
    execute 'inoremap <buffer><silent> '.l:l." <C-R>=Yapp_pair('".l:lk."')<CR>"
    if l:l != l:r
        let l:rk = substitute(l:r, "'", "''", 'g')
        execute 'inoremap <buffer><silent> '.l:r." <C-R>=Yapp_pair('".l:rk."')<CR>"
    endif
endfunction


function! <SID>create_plug_map(key, name, old) abort " {{{1
    let l:i = maparg(a:key, 'i', 0, 1)
    if empty(l:i)
        let l:m = a:key
        let l:e = 0
    else
        let l:m = l:i['rhs']
        let l:e = l:i['expr']
        let l:m = substitute(l:m, '\(<Plug>([^)]*)\)', '\=maparg(submatch(1), "i")', 'g')
        let l:m = substitute(l:m, '<SID>', '<SNR>'.l:i['sid'].'_', 'g')
        let l:w = '<SID>'.a:old
    endif
    if l:m !~? a:name
        if l:e
            execute 'inoremap <buffer> <expr> <script> '.l:w.' '.l:m
            let l:m = l:w
        endif
        execute 'inoremap <script> <buffer> <silent> '.a:key.' '.l:m.'<SID>'.a:name
    endif
endfunction


" Defaults {{{1

let g:yapp_bind_tag        = exists('g:yapp_bind_tag')        ? g:yapp_bind_tag        : 1
let g:yapp_bind_enter      = exists('g:yapp_bind_enter')      ? g:yapp_bind_enter      : 1
let g:yapp_bind_space      = exists('g:yapp_bind_space')      ? g:yapp_bind_space      : 1
let g:yapp_bind_backspace  = exists('g:yapp_bind_backspace')  ? g:yapp_bind_backspace  : 1
let g:yapp_bind_cursormove = exists('g:yapp_bind_cursormove') ? g:yapp_bind_cursormove : 1
let g:yapp_pairs           = exists('g:yapp_pairs')           ? g:yapp_pairs           : {'(':')', '[':']', '{':'}', "'":"'", '"':'"', '`':'`'}

let s:left  = (v:version > 704) ? "\<C-G>U\<Left>"  : "\<Left>"
let s:right = (v:version > 704) ? "\<C-G>U\<Right>" : "\<Right>"

inoremap <silent> <SID>Yapp_space       <C-R>=Yapp_space()<CR>
imap     <script> <Plug>Yapp_space      <SID>Yapp_space
inoremap <silent> <SID>Yapp_enter       <C-R>=Yapp_enter()<CR>
imap     <script> <Plug>Yapp_enter      <SID>Yapp_enter
inoremap <silent> <SID>Yapp_cursormove  <C-R>=Yapp_cursormove()<CR>
imap     <script> <Plug>Yapp_cursormove <SID>Yapp_cursormove

augroup load_settings
    autocmd!
    autocmd BufEnter * silent call Yapp_try_init()
    autocmd InsertLeave * silent call Yapp_counter('reset')
augroup END

" vim:fdm=marker:sw=4:sts=4:et

